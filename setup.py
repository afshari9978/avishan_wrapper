import os
from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.rst')).read()

setup(
    name='avishan_wrapper',
    version='0.1.0',
    packages=['avishan_wrapper'],
    description='Wrapper for avishan package',
    long_description=README,
    author='Morteza Afshari',
    author_email='afshari9978@gmail.com',
    url='https://gitlab.com/afshari9978/avishan_wrapper',
    license='MIT',
    install_requires=[
        'Django>=2.2',
    ]
)
