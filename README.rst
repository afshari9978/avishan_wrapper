===============
Avishan Wrapper
===============

...
# todo edit it

Quick start
-----------

1. Add "avishan" to end of your INSTALLED_APPS setting like this, and "AvishanMiddleware" to **end** of MIDDLEWARE too::

    INSTALLED_APPS = [
        ...
        'avishan',
    ]

    MIDDLEWARE = [
        ...
        'avishan.middlewares.AvishanMiddleware',
    ]

2. Include the avishan URLconf in your project urls.py like this::

    path('', include('avishan.urls')),

3. Run this commands to create the avishan models::

    python manage.py migrate
    python manage.py avishan_init

4. Now follow part below for more detail about every single usage.


What does it do for me?
-------------------------
- Token based authentication
- Data body parser for POST/PUT/... Http requests
- Exception reports
- Fully implemented third-party libraries like Kavenagar & Chabok
- Automated django admin tools